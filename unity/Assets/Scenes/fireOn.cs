﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireOn : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.up * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        enemy enemy = collision.GetComponent<enemy>();
        if (enemy != null)
        {
            enemy.Die();
        }
        Destroy(gameObject);
    }
}
