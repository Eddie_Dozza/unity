﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveEnemy : MonoBehaviour
{
    [SerializeField]
    private float speed = 3f;
    
    // Update is called once per frame
    void Update()
    {
        switch (Random.Range(1, 5))
        {
            case 1: transform.position += new Vector3(0, speed * Time.deltaTime); break;
            case 2: transform.position -= new Vector3(speed * Time.deltaTime, 0); break;
            case 3: transform.position += new Vector3(speed * Time.deltaTime, 0); break;
            case 4: transform.position -= new Vector3(0, speed * Time.deltaTime); break;
        }

    }
}
