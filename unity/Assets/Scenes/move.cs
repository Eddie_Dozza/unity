﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    private float dist = 5;
    //public Transform player;

    void OnMouseDrag()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, transform.position.y, dist);
        Vector3 objPos = Camera.main.ScreenToWorldPoint(mousePos);
        transform.position = objPos;
    }

}
