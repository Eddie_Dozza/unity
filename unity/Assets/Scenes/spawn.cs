﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    public GameObject enemy;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            Instantiate(enemy, new Vector2(Random.Range(-2.5f, 2.5f), Random.Range(-2.5f, 2.5f)), Quaternion.identity);
            yield return new WaitForSeconds(1.5f);
        }
    }

}
